/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacionarrays;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class OrdenacionArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion;
        int [] array = {};
        Scanner input = new Scanner (System.in);
        do {
            imprimirMenu();
            opcion = input.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Introduzca la longitud deseada para el array");
                    array = inicializarArray(input.nextInt());
                    break;
                case 2:
                    imprimirArray(array);
                    break;
                case 3:
                    array = ordenarArray(array);
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Opción incorrecta, elija de nuevo");
                    break;
            }
        }while(opcion!=5);
    }
    
    private static void imprimirMenu() {
        System.out.println("Ordenación de arrays");
        System.out.println("Elija una opción");
        System.out.println("1) Crear un nuevo array aleatorio");
        System.out.println("2) Imprimir Array");
        System.out.println("3) Ordenar array");
        System.out.println("4) Salir");
    }

    private static int[] inicializarArray(int longitud) {
        int [] aux = new int[longitud];
        for (int i = 0; i<aux.length;i++){
            aux[i] = (int)(Math.random()*101);
        }
        return aux;
    }

    private static void imprimirArray(int[] array) {
        System.out.print(" | ");
        for(int i =0;i<array.length;i++){
          System.out.print(array[i]+" | ");
        }
        System.out.println("");
    }

    private static int[] ordenarArray(int[] array) {
        // Bucle externo.
        // Se irá poniendo cada posición tratada, empezando por la 0,
        // el valor más bajo que se encuentre.
        for ( int i = 0 ; i < array. length - 1 ; i ++ ) {
            // Bucle interno.
            // Se busca entre el resto de posiciones cuál es el valor más bajo.
            for ( int j = i + 1 ; j < array. length ; j ++ ) {
                // La posición tratada tiene un valor más alto que el de la búsqueda. Los intercambiamos.
                if ( array [i] > array [j] ) {
                    // Para intercambiar valores hay una variable auxiliar.
                    int aux = array [i] ;
                    array [i] = array [j] ;
                    array [j] = aux ;
                }
            }  
        }
        System.out.println("Array ordenado de menor a mayor, imprima para ver resultados");
        return array;
    }
    
}
